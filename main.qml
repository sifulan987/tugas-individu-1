import Felgo 3.0
import QtQuick 2.0

App {
    NavigationStack {
        Page{
            title : qsTr("Main Page")

            Image {
                id: image
                source : "../assets/felgo-logo.png"
                anchors.centerIn : parent

            }

            AppButton {
                text : "Image Rotation : " + image.rotation + "0"

                onClicked : {
                    image.rotation += 10
                }
            }
        }
    }
}